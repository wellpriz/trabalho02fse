# Trabalho02fse



[Enunciado](https://gitlab.com/fse_fga/trabalhos-2022_2/trabalho-2-2022-2)


# Trabalho 2 - 2022/2

Repositório para Trabalho 2 da disciplina de Fundamentos de Sistemas Embarcados (FGA-UnB). 

## Dependências do trabalho

---

* [WiringPi](https://github.com/WiringPi/WiringPi)
* gcc
* make

## Como executar

---

Primeiramente no terminal dentro da pasta do projeto devemos compilar usando o comando abaixo:

```bash
make all
```

Caso ja exista um copilação utilize o comando abaixo para limpar copilações existentes:

```bash
make clear
```

Para executar, após a copilação use o comando abaixo:

```bash
make run
```

## Utilização

---

Para utilização digite o numero da opção desejada e tecle ENTER.

É necessário configurar o controle PID (Proporcional Integral Derivativo) logo ao iniciar o programa, existem configurações padrões existentes para os ambientes que foram disponibilizados na disciplina.
Porém caso queira é possivel entra com valores custumizados de PID.

Após configurado o PID escolha um modo de operação:

- TEMINAL: É passado um valor de  temperatura de referência e o sistema trabalha para alcançar essa temperatura até sua interrupção.É possível verificar no dashboard o comportamento do sitema e dar comandos por meio dele.
Nesse modo as flag de debug são atividas e valores Temperatura interna, sinal de  referência e Sinal de contrele são mostrados no terminal.
 
- POTENCIOMETRE : Neste modo o sistema usara por padrão a temperatura do Dashboard, ao ser acionado no terminal esse modo seta a temperatura de referência, porém aguarda o botaão iniciar ser acionado para começar a comanda os acionadores de potência. podendo ser posteriormente mudada para curva reflow ao acionar o botão correspondente;

- CURVA: Neste modo o sistema usara por padrão a temperatura do arquivo CURVA.CSV, mudando de tremperatura de controle ao passar o tempo cadastrado no arquivo.podendo ser posteriormente mudada para outro modo ao acionar o botão correspondente;

Ambos os modos quando pressionado o botão de desligar o sistema e desligado.


## LOG

---

O sistema gerará um arquivo log.csv que guarda as temperaturas Tr, Ti e Te, sinal de controle da resistência e ventoinha, além da data e horário.



## DASHBOARD

---

Essa é a interface Dashboard do  sistema,  disponibilizada para realização do trabalho:
No Dashboard são apresentado dados gerado pelo sistema onde são plotados os gráficos de temperatura e sinal de controle.



![Dashboard Interface](./figuras/Dashboard.png)

                                                                               


## Terminal

---
Ao ser inicializado o sistema ira solicitar configurações:
```

## INICIALIZANDO! ###

Deseja utilizar um predefinicao para configurar as constantes do PID?

        (1) Predefinicao rasp42 -> Kp=30.0 Ki=0.2 Kd=400.0.
        (2) Predefinicao customizada.

=======> 1

```
Logo apos ira solicitar o modo de operação:

```
Selecione o modo de temperatura de referencia:


        (1) Terminal.
        (2) POTENCIOMETRE.
        (3) CURVA.
=======> 1

```

No terminal é exibido as seguintes informações:

No modo Terminal será solicitado que entre com uma temperatua de referência.
```

Digite a temperatura de referencia desejada:


=======> 30

```
O sistema sera iniciado em modo DEBUG apresentando as todas as informações:




```
 
### Executando rotina! ###
### COMANDO  # 0 # RECEBIDO! ###
### ----- ESTADO DO SISTEMA ---------: INICIADO
### ----- MODO DE CONTROLE DE TEMPERATURA ---------: MODO MANUAL

### --------------------------------------------------------#####: 

Temperatura de referencia: 30.0 C'

Temperatura interna:       26.2 C'

Sinal de controle:         100.0%

### --------------------------------------------------------#####:        100.0%
```

Além da rotina padrão será exibido quando foi pressionado algum botão da interface:

```
### PRESSIONADO LIGAR! ###
### PRESSIONADO DESLIGAR! ###
### PRESSIONADO PONTECIOMETRO! ###
### PRESSIONADO CURVA REFLOW! ###
```

Nos modos controlados pela Dashboard (CURVA e POTENCIOMETRE) as informações no terminal são resumidas pois são disponibilizadas no Dashboard: 

```
### Executando rotina! ###
### ----- ESTADO DO SISTEMA ---------: PARADO
### ----- MODO DE CONTROLE DE TEMPERATURA ---------: MODO MANUAL


```


## Testes

---

Os testes foram feitos durante 10 minutos para cada forma do modo curva, e potenciômetro e arquivo curva reflow.

O sinal de controle para temperatura varia entre -100 e 100:

 Sinal de controle | Operação do sistema            
-------------------|--------------------------------
 100               | Resistência na potência máxima 
 0                 | Ambos desligados               
 -100              | Ventoinha na potência máxima   

Vale destacar que a ventoinha sempre executa uma velocidade maior que 40 (velocidade mínima), é possível notar esse comportamento com o arquivo log.csv.

Teste utilizando modo  arquivo curva reflow (Testado na rasp47 com a configuração PID padrão do sistema para o mesmo):

| Temperaturas Curvas                                                   |
|-------------------------------------------------------------|
| ![UARTCURV - Temperaturas](./figuras/curva.png) | 
                                 24/01/2023

| Sinal de controle                                                   |
|---------------------------------------------------------------------|
| ![UARTCURV - Sinal de controle](./figuras/sinalcontole-curvas.png) |
                                 24/01/2023

Teste  utilizando modo potenciômetro (Testado na rasp47 com a configuração PID padrão do sistema para o mesmo):

| Temperaturas Potenciômetro                                      |
|-----------------------------------------------------------|
| ![UARTPOT - Temperaturas](./figuras/potenciometre.png )|
                                 24/01/2023

| Sinal de controle                                                 |
|-------------------------------------------------------------------|
| ![UARTPOT - Sinal de controle](./figuras/sinalcontole-potenci.png )|
                                 24/01/2023

# Vídeo de Apresentação



[<img src="./figuras/video.png" width="100%">](./figuras/Apresentacao-Trabalho02-FSE.mp4) 



# Enunciado do Trabalho


## 1. Objetivos

Este trabalho tem por objetivo a implementação de um sistema (que simula) o controle de um forno para soldagem de placas de circuito impresso (PCBs). 

Abaixo vemos alguns exemplos de fornos comerciais para este propósito.

Reflow Oven - iTECH RF-A350  | Forno LPKF Protoflow S4  |  Controleo3 Reflow Oven
:-------------------------:|:-------------------------:|:-------------------------:
<img src="https://cdn.shopify.com/s/files/1/0561/1136/6234/products/ReflowOvenMachine7_3063a9f2-2046-4d42-b79f-b0d3c27bc755_700x.jpg?v=1659768134" data-canonical-src="https://cdn.shopify.com/s/files/1/0561/1136/6234/products/ReflowOvenMachine7_3063a9f2-2046-4d42-b79f-b0d3c27bc755_700x.jpg?v=1659768134" width="200"/> | <img src="https://www.lpkf.com/fileadmin/mediafiles/_processed_/1/b/csm_lpkf_protoflow_s4_05300933fa.png" data-canonical-src="https://www.lpkf.com/fileadmin/mediafiles/_processed_/1/b/csm_lpkf_protoflow_s4_05300933fa.png" width="200"/> | <img src="https://www.whizoo.com/i/c3/c3_oven.jpg" data-canonical-src="https://www.whizoo.com/i/c3/c3_oven.jpg" width="150"/>

No trabalho, o aluno deverá desenvolver o software que efetua o controle de temperatura do forno utilizando dois atuadores para este controle: um **resistor de potência** de 15 Watts utilizado para aumentar temperatura e; uma **ventoinha** que puxa o ar externo (temperatura ambiente) para reduzir a temperatura do sistema. 

Os comandos do usuário do sistema para definir a temperatura desejada serão controlados de duas maneiras:
1. Manualmente através do seletor de temperatura no dashboard (Thingsboad);
2. Automaticamente seguindo uma curva de temperatura pré-definida em arquivo de configuração ([Arquivo da Curva](./curva_reflow.csv)).

O controle da temperatura será realizado através da estratégia PID onde deverão estar disponíveis para o usuário o ajuste dos parâmetros Kp, Ki e Kd nas configurações do sistema (Via terminal).


## 2. Componentes do Sistema

O sistema é composto por:
1. Ambiente fechado controlado com o resistor de potência e ventoinha;
2. 01 Sensor DS18B20 (1-Wire) para a medição da temperatura interna (**TI**) do sistema;
3. 01 Sensor BME280 (I2C) para a medição da temperatura externa (**TE**);
4. 01 Conversor lógico bidirecional (3.3V / 5V);
5. 01 Driver de potência para acionamento de duas cargas (L297);
6. 01 ESP32;
7. 01 Raspberry Pi 4;

![Figura](./figuras/Figura_Trabalho_2_ReflowOven.png)

## 3. Conexões entre os módulos do sistema

1. O sensor de temperatura BME280 está ligado ao barramento I2C da Raspberry Pi e utiliza o endereço (0x76);
2. O resistor de potência e a ventoinha estão ambos ligados às portas GPIO e são acionados através do circuito de potência:  
    2.1. Resistor: GPIO 23;  
    2.2. Ventoinha: GPIO 24.
3. A ESP32 está conectada à placa Raspberry Pi via UART (Protocolo MODBUS-RTU);
5. Os comandos de acionamento e controle do sistema virão do Dashboard (Thingsboard) através da ESP32;
6. O sensor de temperatura DS18B20 para medição do ambiente controlado está ligado à ESP32 na porta GPIO 4 via protocolo 1-Wire;

## 4. Requisitos

Os sistema de controle possui os seguintes requisitos:
1. O código deve ser desenvolvido em C/C++ ou Python;
2. Na implementação do software, não podem haver loops infinitos que ocupem 100% da CPU;
3. O sistema deve implementar o controle de temperatura do ambiente fechado utilizando o controle PID atuando sobre o Resistor e a Ventoinha;
4. A interface de terminal do usuário deve prover a capacidade de definição dos seguintes parâmetros:
   1. **Temperatura de referência (TR)**: deve haver uma opção para escolher se o sistema irá considerar TR definido pelo teclado (Modo Debug), pelo valor definido no dashboard via UART ou seguindo as curvas de temperatura pré-definidas em arquivo.
   2. **Parâmetros Kp, Ki, Kd**: para o controle PID deve ser possível definir os valores das constantes Kp, Ki e Kd;
5. No caso da temperatura ser definida via UART, o programa deve consultar o valor através da comunicação UART-MODBUS com a ESP32 a cada 1 segundo;
6. O programa deve gerar um log em arquivo CSV das seguintes informações a cada 01 segundo com os seguintes valores: (Data e hora, temperatura interna, temperatura externa, temperatura definida pelo usuário, valor de acionamento dos atuadores (Resistor e Venoinha em valor percentual)).
7.  O programa deve tratar a interrupção do teclado (Ctrl + C = sinal **SIGINT**) encerrando todas as comunicações com periféricos (UART / I2C / GPIO) e desligar os atuadores (Resistor e Ventoinha);
8. O sistema deve conter em seu README as instruções de compilação e uso, bem como gráficos* com o resultado de dois experimentos, um com a temperatura de referência fixa e outro seguindo uma curva pré-definida.
 
Obs: 
\* Serão necessários dois gráficos para cada experimento. Um deles plotando as temperaturas (Ambiente, Interna e Referência (Potenciômetro)) e outro gráfico com o valor do acionamento dos atuadores (Resistor / Ventoinha) em valor percentual entre -100% e 100%.

## 5. Comunicação UART com a ESP32

A comunicação com a ESP32 deve seguir o mesmo protocolo MODBUS utilizado no [Exercício 2](https://gitlab.com/fse_fga/exercicios/exercicio-2-uart-modbus).
 
A ESP32 será responsável por:
1. Efetuar a medição da temperatura interna (Sensor DS18B20);
2. Realizar a comunicação com o dashboard (Thingsboard) para definir:
   2.1 O valor da temperatura de referência;
   2.2 Os comandos de ligar e desligar o forno;
   2.3 Os comandos de inicar e parar o aquecimento;
   2.4 Os comandos para a mudança de modo de controle (Temperatura de Referência ou Curvas de Temperatura);
3. Atualizar informações sobre as temperaturas e o sinal de controle no dashboard (ThingsBoard).

Para acessar as informações via UART envie mensagens em formato MODBUS com o seguinte conteúdo:

1. Código do Dispositivo no barramento: 0x01 (Endereço da ESP32);  
2. Leitura do Valor de Temperatura Interna (TI): Código 0x23, Sub-código: 0xC1 + 4 últimos dígitos da matrícula. O retorno será o valor em Float (4 bytes) da temperatura interna do sistema com o pacote no formato MODBUS;
3. Leitura da temperatura de referência - TR (Potenciômetro): Código 0x23, Sub-código: 0xC2 + 4 últimos dígitos da matrícula. O retorno será o valor em Float (4 bytes) da temperatura de referência definida pelo usuário com o pacote no formato MODBUS;
4. Envio do sinal de controle (Resistor / Ventoinha): Código 0x16,  Sub-código: 0xD1 + 4 últimos dígitos da matrícula, Valor em Int (4 bytes).
5. Envio do sinal de referência nos casos em que o sistema esteja sendo controlado ou pelo terminal ou pela curva de referência: Código 0x16,  Sub-código: 0xD2 + 4 últimos dígitos da matrícula, Valor em Float (4 bytes).
6. Leitura dos Comandos de usuário: Código 0x23,  Sub-código: 0xC3;
7. Envio do estado interno do sistema em resposta aos comandos de usuário:
   1. Estado (Ligado / Desligado): Código 0x16,  Sub-código: 0xD3 + byte;
   2. Modo de Controle (Referência Fixa = 0 / Curva de Temperatura = 1): Código 0x16,  Sub-código: 0xD4 + byte;

<p style="text-align: center;">Tabela 1 - Códigos do Protocolo de Comunicação</p>

| Endereço da ESP32 | Código |	Sub-código + Matricula | Comando de Solicitação de Dados |	Mensagem de Retorno |
|:-:|:-:|:-:|:--|:--|
| **0x01** | **0x23** | **0xC1** N N N N |	Solicita Temperatura Interna  | 0x00 0x23 0xC1 + float (4 bytes) |
| **0x01** | **0x23** | **0xC2** N N N N |	Solicita Temperatura de Referência	| 0x00 0x23 0xC2 + float (4 bytes) |
| **0x01** | **0x23** | **0xC3** N N N N |	Lê comandos do usuário  | 0x00 0x23 0xC3 + int (4 bytes de comando) | 
| **0x01** | **0x16** | **0xD1** N N N N |	Envia sinal de controle Int (4 bytes) | 0x00 0x16 0xD1 |
| **0x01** | **0x16** | **0xD2** N N N N |	Envia sinal de Referência Float (4 bytes) | 0x00 0x16 0xD2 |
| **0x01** | **0x16** | **0xD3** N N N N |	Envia Estado do Sistema (Ligado = 1 / Desligado = 0) | 0x00 0x16 0xD3 + int (4 bytes de estado) | 
| **0x01** | **0x16** | **0xD4** N N N N |	Modo de Controle da Temperatura de referência (Dashboard = 0 / Curva/Terminal = 1) (1 byte) | 0x00 0x16 0xD4 + int (4 bytes de modo de controle) | 
| **0x01** | **0x16** | **0xD5** N N N N |	Envia Estado de Funcionamento (Funcionando = 1 / Parado = 0) | 0x00 0x16 0xD5 + int (4 bytes de estado) | 
| **0x01** | **0x16** | **0xD6** N N N N |	Envia Temperatura Ambiente (Float)) | 0x00 0x16 0xD6 + float (4 bytes) | 

**Obs 1**: todas as mensagens devem ser enviadas com o CRC e também recebidas verificando o CRC. Caso esta verificação não seja válida, a mensagem deverá ser descartada e uma nova solicitação deverá ser realizada.    
**Obs 2**: Os comandos D2 e D4 serão usados para o controle da temepratura de referência pelo software embarcado na Raspberry seja no modo 'Curva de Temperatura' ou no modo de 'Debug via Termnal' da aplicação. O 0xD4 determina qual a temperautra de referência que irá aparecer no dashboard se é a configurada pelo usuário nos botões do próprio dashboard ou a temperatura enviada pelo via UART. Já o 0xD2 é o subcomando para envio da temperatura de referência.

<p style="text-align: left;">Tabela 2 - Comandos de Usuário via UART</p>

| Comando | Código |
|:--|:-:|
| **Liga** o Forno | 0xA1 |
| **Desliga** o Forno | 0xA2 |
| **Inicia** aquecimento | 0xA3 |
| **Cancela** processo | 0xA4 |
| **Menu** : alterna entre o modo de Temperatura de Referência e Curva de Temperatura | 0xA5 |  

A leitura dos comandos via UART deve ser realizada a cada **500 ms**.

## 5. Parâmetros de PID

Para o uso do controle do PID, estão sendo sugeridos os seguintes valores para as constantes:


- **Kp** = 30.0
- **Ki** = 0.2
- **Kd** = 400.0
  

Porém, vocês estão livres para testar outros valores que sejam mais adequados.

### Acionamento do Resistor 

O **resistor** deve ser acionado utilizando a técnica de PWM (sugestão de uso da biblioteca WiringPi / SoftPWM). A intensidade de acionamento do resistor por variar entre 0 e 100%.

### Acionamento da Ventoinha

A **venotinha** também deve ser acionada utilizando a técnica de PWM. Porém, há um limite inferior de 40% de intensidade para seu acionamento pelas características do motor elétrico. Ou seja, caso o valor de saída do PID esteja entre 0 e -40%, a ventoinha deverá ser acionada com 40% de sua capacidade.

Observa-se ainda que a saída negativa só indica que o atuador a ser acionado deve ser a ventoinha e não o resistor e o valor de PWM a ser definido deve ser positivo, invertendo o sinal.


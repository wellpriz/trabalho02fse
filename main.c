#include "inc/bme280_driver.h"
#include "inc/pid.h"
#include "inc/csv.h"
#include "inc/esp32_control.h"



#include <stdlib.h>
#include <sys/time.h>
#include <signal.h>
#include <wiringPi.h>
#include <stdio.h>    
#include <softPwm.h>
#include <unistd.h>
#include <pthread.h>


#define MODE_TERMINAL       0x00
#define MODE_UART			0x01

#define UART_MODE_POTENTIOMETER 0x00
#define UART_MODE_CURVE         0x01

#define CSV_LINES_MAX       50 
#define CSV_START_LINE      1      


struct csv_log_data log_data;
float reference_temperature = 0;
float internal_temperature=0;
double control_temperature=0;

int mode = 0;
int uart = 0;
int sistemON = 0;
int curva_state = 0;
int debug =1;

const int RESISTOR_PIN =    4;  /* GPIO23 */
const int FAN_PIN =         5;	/* GPIO24 */

long curve_lines = 0;
float curve_temperature[CSV_LINES_MAX];
long curve_time[CSV_LINES_MAX];
long curve_line = CSV_START_LINE;
long long curve_clock_last = 0;

pthread_t thread_id_log_csv;

/* float temperatura_historico[3] = {0.0, 0.0, 0.0}; */

void exit_thread(){
    pthread_exit(NULL);
}

long long timeInMicroseconds(void){

    struct timeval tv;
    gettimeofday(&tv, NULL);
    return ((((long long)tv.tv_sec)*1000000)+(tv.tv_usec));
}



/* float temperatura_historicoMedia(float temperatura_atual){
    temperatura_historico[0] = temperatura_historico[1];
    temperatura_historico[1] = temperatura_historico[2];
    temperatura_historico[2] = temperatura_atual;
    return (temperatura_historico[0] + temperatura_historico[1] + temperatura_historico[2]) / 3.0;
} */


void esfria(double potencia){
    if(potencia >= -40){
        softPwmWrite(FAN_PIN, 40);
        softPwmWrite(RESISTOR_PIN, 0);

        log_data.fan_speed = 40;
        log_data.resistor_power = 0;
    }else{
        softPwmWrite(FAN_PIN, (int)-potencia);
        softPwmWrite(RESISTOR_PIN, 0);

        log_data.fan_speed = -potencia;
        log_data.resistor_power = 0;

    }
}

void esquenta(double potencia){
    
    log_data.resistor_power = potencia;
    log_data.fan_speed = 0;

    softPwmWrite(RESISTOR_PIN, potencia);
    softPwmWrite(FAN_PIN, 0);   
}



void shutdown(){
    printf("### DESLIGANDO! ###\n");
    send_on_off(ESP_SEND_OFF);
    send_control_mode(ESP_SEND_MODE_POTENTIOMETER);
    send_D5_state_fun_sistem(ESP_SEND_OFF);
    pthread_kill(thread_id_log_csv, SIGUSR1);

    sleep(3);
    esfria(-100);
    sleep(3);
    softPwmWrite(RESISTOR_PIN, 0);
    softPwmWrite(FAN_PIN, 0);
    bme280_driver_close();

    

    exit(0);
}

void controler_temperature(){

   //double last_control_temperature= control_temperature;
   control_temperature = pid_controle(internal_temperature);
    

    if(control_temperature > 1){
        if(sistemON==1) { 

         esquenta(control_temperature);
         if(debug==1)
            printf("### ----- AQUECIMENTO --------: %s\n", (debug!=0 ? "INICIADO" : "PARADO") );  

        }
    }
    else{
         esfria(control_temperature);
         if(debug==1)
            printf("### ----- RESFRIAMENTO --------: %s\n", (debug!=0 ? "INICIADO" : "PARADO") ); 
    }
    send_control_tempetature(control_temperature);

    if(debug==1){
        printf("### ----- ESTADO DO SISTEMA ---------: %s\n", (debug!=0 ? "INICIADO" : "PARADO") );   
        printf("### ----- MODO DE CONTROLE DE TEMPERATURA ---------: %s\n", (curva_state!=0 ? "MODO CURVA " : "MODO MANUAL") );

        printf("\n### --------------------------------------------------------#####: \n");
        printf("\nTemperatura de referencia: %2.1lf C'\n", reference_temperature);
        printf("\nTemperatura interna:       %2.1lf C'\n", internal_temperature);
        printf("\nSinal de controle:         %2.1lf%%\n", control_temperature);
        printf("\n### --------------------------------------------------------#####: \n ");
    }else{
        printf("### ----- ESTADO DO SISTEMA ---------: %s\n", (sistemON!=0 ? "INICIADO" : "PARADO") );   
        printf("### ----- MODO DE CONTROLE DE TEMPERATURA ---------: %s\n", (curva_state!=0 ? "MODO CURVA " : "MODO MANUAL") );
    }
}


void user_routine(){
    Byte command;
    request_user_command(&command);
    if(debug==1)
    printf("### COMANDO  # %x # RECEBIDO! ###\n",command);

    switch(command){

    case ESP_USER_COMANDO_LIGAR:
       
        printf("\n### PRESSIONADO LIGAR! ###\n");
        sistemON=1;
        send_on_off(ESP_SEND_ON);
        break;
    case ESP_USER_COMANDO_DESLIGAR:
       
        printf("\n### PRESSIONADO DESLIGAR! ###\n");
        send_D5_state_fun_sistem(ESP_SEND_OFF);
        esfria(-100.0);
        sistemON=0;
        shutdown();
        break;
    case ESP_USER_COMANDO_INICIA_AQUEC:
       
        printf("\n### PRESSIONADO INICIAR! ###\n");
        mode = MODE_UART;
        uart = UART_MODE_POTENTIOMETER;    
        sistemON=1;
        
        send_D5_state_fun_sistem(ESP_SEND_ON);
        
        //send_D4_control_temperature_ref(ESP_SEND_MODE_POTENTIOMETER);
        break;

    case ESP_USER_COMANDO_CANCELA_AQUEC:
       
        printf("\n ### PRESSIONADO CANCELA! ###\n");
        sistemON = 0 ;
        send_D5_state_fun_sistem(ESP_SEND_OFF);
        uart = UART_MODE_POTENTIOMETER;
        send_control_mode(ESP_SEND_MODE_POTENTIOMETER);
        curva_state=UART_MODE_POTENTIOMETER;
        
        break;

    case ESP_USER_COMANDO_CURVE:
         if(curva_state==UART_MODE_POTENTIOMETER){
            printf("\n### PRESSIONADO CURVA REFLOW! ###\n");
            //curve_lines = csv_read_csv_curve(curve_temperature, curve_time); 
            mode = MODE_UART;
            sistemON=1;
            send_D5_state_fun_sistem(ESP_SEND_ON);
            curve_clock_last = timeInMicroseconds();
            curve_line = CSV_START_LINE;
            curva_state = UART_MODE_CURVE;
            
            //send_control_mode(ESP_SEND_ON);
            send_control_mode(ESP_SEND_ON);
            uart = UART_MODE_CURVE;
            break;
        }
        if(curva_state == UART_MODE_CURVE){
            mode = MODE_UART;
            printf("\n### PRESSIONADO MANUAL! ###\n");
            sistemON=0;
            send_D5_state_fun_sistem(ESP_SEND_OFF);
            send_control_mode(ESP_SEND_OFF);
            
            curva_state = UART_MODE_POTENTIOMETER;
            uart = UART_MODE_POTENTIOMETER;
            break;
        }
        default:
        break;
}
}
void atualiza_tem(){
    log_data.reference_temperature = reference_temperature;
    log_data.internal_temperature = internal_temperature;
    bme280_get_temperature(&log_data.external_temperature);
    send_D6_tempetature_ambient(log_data.external_temperature);
}

void control_temperature_routine(){
    
    if(mode == MODE_UART){
        if(uart == UART_MODE_POTENTIOMETER){
            request_potentiometer(&reference_temperature);
        }else if(curve_line < curve_lines){
            if((timeInMicroseconds() - curve_clock_last)/1000000 >= curve_time[curve_line]){
                reference_temperature = curve_temperature[curve_line++];
                send_reference_tempetature(reference_temperature);
                sleep(1);
            }
            
            if(curve_line == curve_lines){
                printf("### CURVE REFLOW TERMINOU! ###\n");
                printf("### CURVE REFLOW TERMINOU! ###\n");
                printf("### CURVE REFLOW TERMINOU! ###\n");
                printf("### CURVE REFLOW TERMINOU! ###\n");
                printf("### CURVE REFLOW TERMINOU! ###\n");
                printf("### CURVE REFLOW TERMINOU! ###\n");
                printf("### CURVE REFLOW TERMINOU! ###\n");
                printf("### CURVE REFLOW TERMINOU! ###\n");

                uart = UART_MODE_POTENTIOMETER;
                sistemON= 0;
                
                sleep(1);
            }
        }
    }

    //log_data.reference_temperature = reference_temperature;
	send_reference_tempetature(reference_temperature);
    pid_atualiza_referencia(reference_temperature);
    if(reference_temperature<1){
        pid_atualiza_referencia(reference_temperature);
    }
   
   float last_internal_temperature = internal_temperature;
    request_internal_temperature(&internal_temperature);
    if(internal_temperature<1){
        request_internal_temperature(&internal_temperature);
    }
    while(  (internal_temperature > last_internal_temperature+2) ||  (internal_temperature < last_internal_temperature-2)  )
    {
        if(debug==1)
        printf("\n### Erro na temp %f! ###\n",internal_temperature);

        last_internal_temperature = internal_temperature;
        request_internal_temperature(&internal_temperature);

        sleep(1);
        }
    


    controler_temperature();
    atualiza_tem();
    
    
}

void loop_routine(){
    printf("\n### Executando rotina! ###\n");
    modbus_open();
    user_routine();
    control_temperature_routine();
    modbus_close();
    
}

void* loop(){

    long long wait = 0;
    long long currentTime = 0;
    long long lastTime = timeInMicroseconds();
    pthread_t id = pthread_self();

    while(1){
        currentTime = timeInMicroseconds();
        
        if(pthread_equal(id, thread_id_log_csv)){
            csv_append_log(log_data);
            wait += 1000000 - (currentTime - lastTime);
        }else{
            loop_routine();
            wait += 500000 - (currentTime - lastTime);
        }

        if(wait > 0){
            usleep(wait);
		    wait = 0;
        }
        lastTime = timeInMicroseconds();
    }
}

void scan_mode(){

    char option;

    printf("\nSelecione o modo de temperatura de referencia:\n\n");
    do{
        printf("\n\t(1) Terminal.\n");
        printf("\t(2) POTENCIOMETRE.\n");
        printf("\t(3) CURVA.\n");
        printf("=======> ");
        scanf(" %c", &option);

        switch(option){
        case '1':
			printf("\nDigite a temperatura de referencia desejada:\n\n");
			printf("=======> ");
			scanf(" %f", &reference_temperature);
			mode = MODE_TERMINAL;
             debug=1;
            break;

        case '2':
			mode = MODE_UART;
            sistemON=1;
            curva_state = 0;
            send_control_mode(ESP_SEND_MODE_POTENTIOMETER);
             debug=0;

            break;

        case '3':
			mode = MODE_UART;
            uart = ESP_SEND_MODE_CURVE;
            sistemON=1;
            curva_state = 1; 
            send_control_mode(ESP_SEND_MODE_CURVE);
            curve_clock_last = timeInMicroseconds();
            curve_line = CSV_START_LINE;
            curva_state = UART_MODE_CURVE;
            debug=0;
            break;
        default:
            printf("\nEntrada invalida! Selecione novamente:\n");
            option = '\0';
        }
    }while(option == '\0');
    
}

void scan_pid_consts(){

	double Kp, Ki, Kd;
	char option;

	printf("\nDeseja utilizar um predefinicao para configurar as constantes do PID?\n");
    do{
        printf("\n\t(1) Predefinicao rasp42 -> Kp=30.0 Ki=0.2 Kd=400.0.\n");
        printf("\t(2) Predefinicao customizada.\n\n");
        printf("=======> ");
        scanf(" %c", &option);

        switch(option){
        case '1':
            Kp = 30.0, Ki = 0.2, Kd = 400.0;
            break;

        case '2':
            printf("\nInforme a constantes Kp Ki Kd para o PID:\n\n");
            printf("=======> ");
            scanf("%lf %lf %lf", &Kp, &Ki, &Kd);
            break;

        default:
            printf("\nEntrada invalida! Selecione novamente:\n");
            option = '\0';
        }
    }while(option == '\0');
    pid_configura_constantes(Kp, Ki, Kd);
}

int main(void){

    signal(SIGINT, shutdown);
    signal(SIGUSR1, exit_thread);
    //setando estado inicial do sistema

    printf("### INICIALIZANDO! ###\n");

    if(wiringPiSetup() == -1) exit(1);
    
    if(bme280_driver_init() != BME280_OK) exit(1);  
    
    

    pinMode(RESISTOR_PIN, OUTPUT);
	pinMode(FAN_PIN, OUTPUT);
	softPwmCreate(RESISTOR_PIN, 1, 100);
    softPwmCreate(FAN_PIN, 1, 100);

    modbus_open();
    send_on_off(ESP_SEND_OFF);
    send_control_mode(ESP_SEND_MODE_POTENTIOMETER);
    send_D5_state_fun_sistem(ESP_SEND_OFF);

	scan_pid_consts();
	scan_mode();
    csv_create_log();

    if(mode == MODE_TERMINAL){
        send_control_mode(ESP_SEND_MODE_CURVE);
    }else{
        send_control_mode(ESP_SEND_MODE_POTENTIOMETER);
        curve_lines = csv_read_csv_curve(curve_temperature, curve_time);    
    }

    send_on_off(ESP_SEND_ON);
    modbus_close();
    sleep(1);
    printf("### SISTEMA LIGADO! ###\n");

    pthread_create(&thread_id_log_csv, NULL, loop, NULL);
    loop();

    return 0;
}
